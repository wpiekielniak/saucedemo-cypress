## Swag Labs Demo QA

Swag Labs Demo Cypress Automation

### Install project dependencies
- `npm install`

### Run tests
- `npm run test`
##### with Chrome
- `npm run test:chrome`

### Generate and view report
- `npm run report:generate && npm run report:open`

</br><p align="center">
<img src="report-1.png"/></br>
<img src="report-2.png"/></br>
