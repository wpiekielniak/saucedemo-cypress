import { defineConfig } from 'cypress';
import { addCucumberPreprocessorPlugin } from '@badeball/cypress-cucumber-preprocessor';
import createBundler from '@bahmutov/cypress-esbuild-preprocessor';
import { NodeModulesPolyfillPlugin as nodePolyfills } from '@esbuild-plugins/node-modules-polyfill';
import allureWriter from '@shelex/cypress-allure-plugin/writer';

const createEsbuildPlugin =
    require('@badeball/cypress-cucumber-preprocessor/esbuild').createEsbuildPlugin;

export default defineConfig({
    defaultCommandTimeout: 20000,
    pageLoadTimeout: 120000,
    requestTimeout: 10000,
    responseTimeout: 60000,
    execTimeout: 60000,
    taskTimeout: 60000,

    viewportWidth: 1280,
    viewportHeight: 720,

    chromeWebSecurity: false,
    video: false,
    videoCompression: false,

    screenshotsFolder: 'results/screenshots',
    videosFolder: 'results/videos',

    reporter: 'cypress-multi-reporters',
    reporterOptions: {
        configFile: 'reporter-config.json',
    },

    retries: {
        runMode: 1,
        openMode: 1,
    },

    env: {
        allure: true,
        allureResultsPath: 'results/allure',
        allureLogCypress: true,
        allureAttachRequests: true,
        allureLogGherkin: true,
        allureAddAnalyticLabels: true,
        TAGS: 'not @ignore',
    },

    e2e: {
        baseUrl: 'https://www.saucedemo.com',
        specPattern: 'cypress/e2e/**/*.{feature,features}',
        async setupNodeEvents(on, config) {
            await addCucumberPreprocessorPlugin(on, config);
            on(
                'file:preprocessor',
                createBundler({
                    plugins: [nodePolyfills(), createEsbuildPlugin(config)],
                }),
            );
            allureWriter(on, config);
            return config;
        },
    },
});