@store
Feature: Checkout
  As a User I want to purchase a product

  Background:
    Given I'm on the login page
    And I log in as standard user
    When I'm on the store page
    Then I see list of products

  Scenario: Add random product to cart and proceed to checkout
    When I add random product to cart
    And I should see cart badge
    And I go to cart page
    And I see cart is not empty
    Then I go to checkout page
    And I fill checkout info:
      | firstName | lastName | postalCode |
      | test      | test     | 12345      |
    And I go to checkout overview page
    And I should see summary info
    When I finish checkout
    Then I should be redirected to checkout complete page