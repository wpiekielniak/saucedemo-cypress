import { Then, When } from '@badeball/cypress-cucumber-preprocessor';
import { storePage } from '../pageObjects/Store.page';

Then(/^I'm on the store page$/, function() {
    storePage.verifyElementShouldBeVisible(storePage.shoppingCart);
});
Then(/^I see list of products$/, function() {
    storePage.verifyListShouldNotBeEmpty(storePage.products);
});
When(/^I go to cart page$/, function() {
    storePage.gotoShoppingCart();
});
When(/^I should see cart badge$/, function() {
    storePage.verifyElementShouldBeVisible(storePage.shoppingCartBadge);
});
When(/^I add random product to cart$/, function() {
    storePage.addRandomProductToCart();
});
When(/^I sort products by name in (asc|desc) order$/, function(orderBy: string) {
    storePage.sortProductsByName(orderBy);
});
When(/^I sort products by price in (asc|desc) order$/, function(orderBy: string) {
    storePage.sortProductsByPrice(orderBy);
});
Then(/^I see first product with price (greater|less) than last product$/, function(priceStatus: string) {
    storePage.inventoryPrices.invoke('text').then((prices) => {
        let values = prices.split('$').slice(1);
        if (priceStatus === 'greater') {
            expect(Number(values[0])).greaterThan(Number(values[values.length - 1]));
        } else if (priceStatus === 'less') {
            expect(Number(values[0])).lessThan(Number(values[values.length - 1]));
        }
    });
});
Then(/^I see first product with name "([^"]*)"$/, function(productName: string) {
    storePage
        .verifyElementShouldBeVisible(storePage.products)
        .verifyElementShouldHaveText(storePage.products.get(storePage._productNames).first(), productName);
});