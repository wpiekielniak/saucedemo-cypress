import { When, Then, DataTable } from '@badeball/cypress-cucumber-preprocessor';
import { checkoutPage } from '../pageObjects/Checkout.page';
import { checkoutCompletePage } from '../pageObjects/CheckoutComplete.page';

Then(/^I fill checkout info:$/, function(data: DataTable) {
    const [info] = data.hashes();
    checkoutPage.fillCheckoutInfo(info.firstName, info.lastName, info.postalCode);
});
Then(/^I go to checkout overview page$/, function() {
    checkoutPage.continueCheckout();
});
Then(/^I should see summary info$/, function() {
    checkoutPage.verifyElementShouldBeVisible(checkoutPage.summaryInfo);
});
When(/^I finish checkout$/, function() {
    checkoutPage.finishCheckout();
});
Then(/^I should be redirected to checkout complete page$/, function() {
    checkoutCompletePage
        .verifyUrlShouldContainValue('/checkout-complete')
        .verifyElementShouldBeVisible(checkoutCompletePage.checkoutCompleteContainer);
});