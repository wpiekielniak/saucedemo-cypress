import { When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { cartPage } from '../pageObjects/Cart.page';

When(/^I see cart is not empty$/, function() {
    cartPage.verifyElementShouldBeVisible(cartPage.cartItem);
});
Then(/^I go to checkout page$/, function() {
    cartPage.gotoCheckoutPage();
});