import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import { loginPage } from '../pageObjects/Login.page';
import userData from '../../fixtures/login/users.json';

When(/^I log in as standard user$/, function() {
    loginPage.login(userData.standardUser.username, userData.standardUser.password);
});
When(/^I log in as locked out user$/, function() {
    loginPage.login(userData.lockedOutUser.username, userData.lockedOutUser.password);
});
Then(/^I see error login message$/, function() {
    loginPage
        .verifyElementShouldBeVisible(loginPage.errorMessage)
        .verifyElementShouldBeVisible(loginPage.loginLogo);
});
Given(/^I'm on the login page$/, function() {
    loginPage
        .open()
        .verifyElementShouldBeVisible(loginPage.loginForm);
});