import BasePage from './Base.page';

class CheckoutCompletePage extends BasePage {
    private _checkoutCompleteContainer = '#checkout_complete_container';

    public get checkoutCompleteContainer() {
        return cy.get(this._checkoutCompleteContainer);
    }
}

export const checkoutCompletePage = new CheckoutCompletePage();