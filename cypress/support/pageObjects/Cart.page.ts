import BasePage from './Base.page';

class CartPage extends BasePage {
    private _cartItem = '.cart_item';

    private _checkoutButton = '#checkout';

    public get cartItem() {
        return cy.get(this._cartItem);
    }

    public get checkoutButton() {
        return cy.get(this._checkoutButton);
    }

    public gotoCheckoutPage(): CartPage {
        this.checkoutButton.click();
        return this;
    }
}

export const cartPage = new CartPage();