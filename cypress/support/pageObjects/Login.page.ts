import BasePage from './Base.page';

class LoginPage extends BasePage {
    private _loginLogo = '.login_logo';

    private _usernameInput = '#user-name';

    private _passwordInput = '#password';

    private _loginButton = '#login-button';

    private _errorMessage = '.error-message-container';

    private _loginForm = '.login_wrapper-inner';

    public get loginLogo() {
        return cy.get(this._loginLogo);
    }

    public get usernameInput() {
        return cy.get(this._usernameInput);
    }

    public get passwordInput() {
        return cy.get(this._passwordInput);
    }

    public get loginButton() {
        return cy.get(this._loginButton);
    }

    public get errorMessage() {
        return cy.get(this._errorMessage);
    }

    public get loginForm() {
        return cy.get(this._loginForm);
    }

    public login(username: string, password: string): LoginPage {
        this.usernameInput.type(username);
        this.passwordInput.type(password);
        this.loginButton.click();
        return this;
    }
}

export const loginPage = new LoginPage();