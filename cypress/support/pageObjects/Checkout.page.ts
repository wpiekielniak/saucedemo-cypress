import BasePage from './Base.page';

class CheckoutPage extends BasePage {
    private _firstNameInput = '#first-name';

    private _lastNameInput = '#last-name';

    private _postalCodeInput = '#postal-code';

    private _continueButton = '#continue';

    private _summaryInfo = '.summary_info';

    private _finishButton = '#finish';

    public get firstNameInput() {
        return cy.get(this._firstNameInput);
    }

    public get lastNameInput() {
        return cy.get(this._lastNameInput);
    }

    public get postalCodeInput() {
        return cy.get(this._postalCodeInput);
    }

    public get continueButton() {
        return cy.get(this._continueButton);
    }

    public get summaryInfo() {
        return cy.get(this._summaryInfo);
    }

    public get finishButton() {
        return cy.get(this._finishButton);
    }

    public fillCheckoutInfo(firstName: string, lastName: string, postalCode: string): CheckoutPage {
        this.firstNameInput.type(firstName);
        this.lastNameInput.type(lastName);
        this.postalCodeInput.type(postalCode);
        return this;
    }

    public continueCheckout(): CheckoutPage {
        this.continueButton.click();
        return this;
    }

    public finishCheckout(): CheckoutPage {
        this.finishButton.click();
        return this;
    }

}

export const checkoutPage = new CheckoutPage();