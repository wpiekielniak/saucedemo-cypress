import BasePage from './Base.page';

class StorePage extends BasePage {
    private _shoppingCart = '#shopping_cart_container';

    private _addToCartButton = 'button[id*="add-to-cart"]';

    private _products = '.inventory_list';

    public _productNames = '.inventory_item_name';

    private _shoppingCartBadge = '.shopping_cart_badge';

    private _shoppingCartLink = '.shopping_cart_link';

    private _productSortContainer = '.product_sort_container';

    private _productPrice = '.inventory_item_price';

    public get shoppingCart() {
        return cy.get(this._shoppingCart);
    }

    public get products() {
        return cy.get(this._products);
    }

    public get shoppingCartBadge() {
        return cy.get(this._shoppingCartBadge);
    }

    public get shoppingCartLink() {
        return cy.get(this._shoppingCartLink);
    }

    public get productSortContainer() {
        return cy.get(this._productSortContainer);
    }

    public get inventoryPrices() {
        return this.products.get(this._productPrice);
    }

    public gotoShoppingCart(): StorePage {
        this.shoppingCartLink.click();
        return this;
    }

    public addRandomProductToCart(): StorePage {
        this.products.get(this._addToCartButton).then((buttons) => {
            return Cypress._.sample(buttons.toArray())
        }).click();
        return this;
    }

    public sortProductsByName(orderBy = 'asc'): StorePage {
        if (orderBy === 'asc') {
            this.productSortContainer.select('az');
        } else if (orderBy === 'desc') {
            this.productSortContainer.select('za');
        } else {
            cy.log('Sorting undefined!')
        }
        return this;
    }

    public sortProductsByPrice(orderBy = 'asc'): StorePage {
        if (orderBy === 'asc') {
            this.productSortContainer.select('lohi');
        } else if (orderBy === 'desc') {
            this.productSortContainer.select('hilo');
        } else {
            cy.log('Sorting undefined!')
        }
        return this;
    }
}

export const storePage = new StorePage();