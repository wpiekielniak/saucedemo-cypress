import Chainable = Cypress.Chainable;

export default class BasePage {
    public open(): BasePage {
        cy.visit('/');
        return this;
    }

    public verifyUrlShouldContainValue(value: string): BasePage {
        cy.url().should('contain', value);
        return this;
    }

    public verifyElementShouldBeVisible(element: Chainable<JQuery>): BasePage {
        element.should('be.visible');
        return this;
    }

    public verifyElementShouldHaveText(element: Chainable<JQuery>, expectedText: string): BasePage {
        element.should('have.text', expectedText);
        return this;
    }

    public verifyListShouldNotBeEmpty(list: Chainable<JQuery>): BasePage {
        list.should('not.be.empty');
        return this;
    }
}